#include "Core/Types/CoreTypes.h"
#include "Core/Render/Renderer/Renderer.h"
#include "Core/Render/Window/Window.h"
#include "Polygon/Generator/PolygonDrawInfoGenerator.h"
#include "../Lab1Manager.h"
#include <iostream>
#include <sstream>
#include <iomanip>

std::string FloatToPrecisionString(float Number, int Precision)
{
	std::stringstream stream;
	stream << std::fixed << std::setprecision(Precision) << Number;
	return stream.str();
}

int main(int argc, char** argv)
{
	Window Window(800, 500, "Lab1");

	Renderer Renderer(&Window);
	Renderer.SetBackgroundColor(Color::Black);

	Lab1Manager PentagonsManager(&Window);

	Timer Timer;
	float DeltaTime;

	Vector3d FpsCaptionPosition = Vector3d(0, Window.GetHeight() - 15);

	Renderer.Prepare();
	while (Renderer.IsRunning())
	{
		DeltaTime = Timer.GetElapsedTime();
		Timer.RegisterCurrentTime();

		PentagonsManager.Update();

		// Add pentagons to draw
		for (const Pentagon& Pentagon : PentagonsManager.GetPentagons())
		{
			Renderer.AddToDraw(Pentagon.ShapeInfo);
			Renderer.AddToDraw(Pentagon.TextInfo);
		}

		// Calculate FPS
		float FPS = 1.f / DeltaTime;
		// Add fps caption
		DrawTextInfo FpsText = 
		{ 
			// Hardcoded different colrs for diferent fps
			FPS >= 60.0 ? Color::Green : (FPS >= 30.0 ? Color(0.8, 0.82, 0.0) : Color::Red),
			FpsCaptionPosition, 
			"FPS: " + FloatToPrecisionString(FPS, 0)
		};
		Renderer.AddToDraw(FpsText);

		Renderer.Update();
	}

	return 0;
}