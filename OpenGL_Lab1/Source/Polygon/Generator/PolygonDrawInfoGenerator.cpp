#include "PolygonDrawInfoGenerator.h"
#include "Core/Math/Math.h"
#include <cassert>

constexpr float FullCircleDegrees = 360.f;

PolygonDrawInfoGenerator::PolygonDrawInfoGenerator(int InAnglesCount) : AnglesCount{ InAnglesCount } 
{
	assert(InAnglesCount > 2);
}

PolygonDrawInfoGenerator& PolygonDrawInfoGenerator::GenerateShape(const Vector3d& PolygonCenter, float OuterCircleRadius)
{
	Result.Vertices.clear();

	std::vector<float> Angles = GetRandomNormalizedAngles();

	// Calculate polygon vertices absolute positions
	float AngleShift = 0.f;
	for (float Angle : Angles)
	{
		// Convert polar coordintes to cartesian
		float X = PolygonCenter.X + OuterCircleRadius * Math::CosDeg(AngleShift + Angle);
		float Y = PolygonCenter.Y + OuterCircleRadius * Math::SinDeg(AngleShift + Angle);

		Result.Vertices.push_back({ X, Y });

		AngleShift += Angle;
	}

	return *this;
}

PolygonDrawInfoGenerator& PolygonDrawInfoGenerator::GenerateRandomColor()
{
	Result.Color = Color::MakeRandomColor();

	return *this;
}

std::vector<float> PolygonDrawInfoGenerator::GetRandomNormalizedAngles()
{
	std::vector<float> Angles;

	float Sum = 0.f;
	// Generate random numbers
	for (int i = 0; i < AnglesCount; ++i)
	{
		// Limit rand value to avoid Sum overflow
		float RandNum = Math::RandRange(1.f, 3.f);
		Angles.push_back(RandNum);

		Sum += RandNum;
	}

	// Normalize numbers and calculate degrees
	for (int i = 0; i < AnglesCount; ++i)
	{
		Angles[i] = (Angles[i] / Sum) * FullCircleDegrees;
	}

	return Angles;
}
