#pragma once

#include "Core/Types/CoreTypes.h"
#include "Core/Render/DrawInfo/DrawInfo.h"
#include <vector>

/**
 * This class is builder for convex polygons DrawInfo.
 */
class PolygonDrawInfoGenerator
{
public:
	PolygonDrawInfoGenerator(int InAnglesCount);

	/** Chain setup methods */
	PolygonDrawInfoGenerator& GenerateShape(const Vector3d& PolygonCenter, float OuterCircleRadius);
	PolygonDrawInfoGenerator& GenerateRandomColor();

	DrawInfo Get() { return Result; }

private:
	/** Returns angles, which in sum gives 360 degrees */
	std::vector<float> GetRandomNormalizedAngles();

private:
	int AnglesCount;

	DrawInfo Result;
};

