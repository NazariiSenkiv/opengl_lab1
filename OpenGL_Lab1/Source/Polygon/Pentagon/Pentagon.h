#pragma once

#include <Core/Render/DrawInfo/DrawInfo.h>

struct Pentagon
{
	DrawInfo ShapeInfo;
	DrawTextInfo TextInfo;
};