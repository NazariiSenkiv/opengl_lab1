#include "Lab1Manager.h"

#include "Source/Polygon/Generator/PolygonDrawInfoGenerator.h"
#include "Core/Math/Math.h"
#include "Core/Render/Window/Window.h"
#include <algorithm>
#include <iostream>

Lab1Manager::Lab1Manager(Window* InWindow)
	: PentagonsGenerator{ 5 }, MainWindow{ InWindow }
{
	Timer.RegisterCurrentTime();

	SpawnNewPentagon(0.0);
}

void Lab1Manager::Update()
{
	double CurrentTime = Timer.GetElapsedTime();

	double TimeToSpawn = (LastSpawnTime + PentagonSpawnDelay) - CurrentTime;
	if (TimeToSpawn <= 0.0)
	{
		SpawnNewPentagon(CurrentTime);

		LastSpawnTime = CurrentTime;
	}


	if (TemporaryPentagons.empty())
	{
		return;
	}

	// Delete timed out objects
	for (int i = TemporaryPentagons.size() - 1; i >= 0; --i)
	{
		if (TemporaryPentagons[i].GetLifeEndTime() <= CurrentTime)
		{
			auto CurrentIt = TemporaryPentagons.begin() + i;
			TemporaryPentagons.erase(CurrentIt);
		}
	}
}

std::vector<Pentagon> Lab1Manager::GetPentagons()
{
	std::vector<Pentagon> DrawInfos;

	for (const TLifetimeLimiter<Pentagon>& PentagonContainer : TemporaryPentagons)
	{
		DrawInfos.push_back(PentagonContainer.Content);
	}

	return DrawInfos;
}

void Lab1Manager::SpawnNewPentagon(double SpawnTime)
{
	if (!MainWindow)
	{
		return;
	}

	// Randomize pentagon lifetime
	double PentagonLifetime = Math::RandRange(MinPentagonLifetime, MaxPentagonLifetime);

	// Randomize pentagon position
	double WinWidth = MainWindow->GetWidth();
	double WinHeight = MainWindow->GetHeight();

	double X = Math::RandRange(PentagonOuterCircleRadius, WinWidth - PentagonOuterCircleRadius);
	double Y = Math::RandRange(PentagonOuterCircleRadius, WinHeight - PentagonOuterCircleRadius);

	DrawInfo PentagonDrawInfo = PentagonsGenerator.GenerateShape(Vector3d(X, Y), PentagonOuterCircleRadius)
												  .GenerateRandomColor()
												  .Get();
	
	Color Inversed = PentagonDrawInfo.Color.GetInversed();

	Pentagon Pentagon = { PentagonDrawInfo, { Inversed, Vector3d(X, Y), PentagonDrawInfo.Color.GetHexCode()}};
	TemporaryPentagons.push_back({ {Pentagon}, PentagonLifetime, SpawnTime });
}
