#pragma once

#include <vector>
#include "Source/Polygon/Pentagon/Pentagon.h"
#include "Core/Types/Timer/Timer.h"
#include "Source/Polygon/Generator/PolygonDrawInfoGenerator.h"

class Window;

template<typename T>
struct TLifetimeLimiter
{
	T Content;

	double LifeTime;
	double CreationTime;

	double GetLifeEndTime() const { return LifeTime + CreationTime; }
};

class Lab1Manager
{
public:
	Lab1Manager(Window* InWindow);

	void Update();
	std::vector<Pentagon> GetPentagons();

protected:
	void SpawnNewPentagon(double SpawnTime);


private:
	std::vector<TLifetimeLimiter<Pentagon>> TemporaryPentagons;

	Timer Timer;

	PolygonDrawInfoGenerator PentagonsGenerator;

	Window* MainWindow;

/** Pentagon settings */
#pragma region Pentagon Settings
	float PentagonOuterCircleRadius = 100.0;

	float MinPentagonLifetime = 2.0;
	float MaxPentagonLifetime = 5.0;

	double PentagonSpawnDelay = 1.0;
	double LastSpawnTime = 0.0;
#pragma endregion
};

